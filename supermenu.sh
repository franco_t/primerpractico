#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;

#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual="/home/franco/primerpractico"

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
	
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t f.  Abrir en terminal";        
    echo -e "\t\t\t g.  Abrir en carpeta";
    echo -e "\t\t\t h.  buscar archivo";
    echo -e "\t\t\t i.  sobreescribir y guardar  historial";
    echo -e "\t\t\t j.  Agregar historial";
    echo -e "\t\t\t k.  Ver repositorio";
    echo -e "\t\t\t l-  Nuevo texto";
    
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}
chequear_pull (){
        
    if [ $Primera = true ]; then
         var="";
         cd $proyectoActual;
         git fetch origin
         var=$((git log HEAD..origin/master --oneline)2>&1)
   	 if  test -z "$var"
   	 then
    		PULL="";
		echo -e $PULL;
   	 else  
     	
                echo -e "\t\t\t\t\t\t\t ${red}$PULL${reset}";
	
   	 fi 
    	 Primera=false;
    fi
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	echo "---------------------------"        
	echo "Somthing to commit?"
        decidir "cd $proyectoActual; git status";

        echo "---------------------------"        
	echo "Incoming changes (need a pull)?"
	decidir "cd $proyectoActual; git fetch origin"
	decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

b_funcion () {
       	imprimir_encabezado "\tOpción b.  Guardar cambios";
       	decidir "cd $proyectoActual; git add -A";
       	echo "Ingrese mensaje para el commit:";
       	read mensaje;
       	decidir "cd $proyectoActual; git commit -m \"$mensaje\"";
       	decidir "cd $proyectoActual; git push";
}

c_funcion () {
      	imprimir_encabezado "\tOpción c.  Actualizar repo";
      	decidir "cd $proyectoActual; git pull";   	 
}


f_funcion () {
	imprimir_encabezado "\tOpción f.  Abrir en terminal";        
	decidir "cd $proyectoActual; xterm &";
}

g_funcion () {
	imprimir_encabezado "\tOpción g.  Abrir en carpeta";        
	decidir "gnome-open $proyectoActual &";
}

#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por cada item
#------------------------------------------------------
##
h_funcion () {
       imprimir_encabezado  "\tOpción h.  Buscar archivo";
       decidir "cd $proyectoActual; buscar_palabras_ingresadas";
}
buscar_palabras_ingresadas () {
	echo "¿que archivo desea buscar?";
	read respuesta;
	Variable=ls | grep $respuesta;
	if test -n "$Variable";
	then
		Variable="no esta el contenido";
		echo $Variable;
	else
		 ls | grep $respuesta;
	fi
}

i_funcion () {
	imprimir_encabezado "\tOpción i. Guardar ultimo historial";
	decidir "cd $proyectoActual; GuardarelLog"; 
}

GuardarelLog () {
	echo "guardar y remplazar";
	git log > guardarLog.txt;
}


j_funcion () {
	imprimir_encabezado "\tOpción j. Sumar historial";
	decidir " cd $proyectoActual; Sumarhistorial";
}

Sumarhistorial () {
	git log >> guardarLog.txt;
}

k_funcion () {
	imprimir_encabezado "\tOpción k. ver archivos";
	decidir "cd $proyectoActual; lista";
}

lista () {
	echo "";
	echo "a - archivo existentes";
	echo "b - permisos de los archivos";
	echo "c - archivos ocultos";
	echo "";
	read respuesta;
	var=$respuesta;
	if [ $var = "a" ];
	then
		ls;
	fi
	if [ $var = "b" ];
	then
		ls -l;
	fi

	if [ $var = "c" ];
	then
		ls -a;
	fi
}

l_funcion () {
	imprimir_encabezado "\tOpción l. Nuevo archivo de texto";
	decidir "cd $proyectoActual; crear_archivo_de_texto";
}

crear_archivo_de_texto () {
	ruta=/home/franco/primerpractico;
	echo "";
	echo "escribe el nombre del archivo:";
	read input;
	touch $ruta/$input.txt;
	echo "";
	echo "escribe texto: ";
	echo "para terminar use CTRL+d";
	echo "";
	cat>$input.txt;

}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    #aca puse el punto---de hacer pull
    PULL="Necesita hacer pull";
    Primera=true;
    chequear_pull;
     
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
	h|H) h_funcion;;
	i|I) i_funcion;;
	j|J) j_funcion;;
	k|K) k_funcion;;
	l|L) l_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done

