#include <stdio.h>  //incluimos la libreria de estandar input/output
#include <unistd.h> //para hacer sleep
#include <stdlib.h> //para libreria de numeros random: srand, rand 
#include <time.h>   //para inicializar el tiempo
#include <sys/wait.h> 

void do_nothing(int microseconds, char* mensaje){
  usleep(microseconds); //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n",mensaje);  
   //printf("su pid 2seg: %d ", getpid());
}

void do_nothing_random(char* mensaje){
  srand(time(NULL));                    //inicializar la semilla del generador random:
  int microseconds = rand() % 1000 + 1; //generar un numer random entre 1 y 1000:
  usleep(microseconds);                 //dormir el thread, simula que esta haciendo alguna tarea
  printf("\n %s \n",mensaje);
  //printf("su pid randi: %d ", getpid());
}


int main() {
  char* msg= "hola";
  //char* msg2= "hola2";
  //do_nothing(2000000,msg); //esperar 2 segundos, 1 millon de microsegundos en 1 segundo 
  //do_nothing_random(msg);  //esperar un tiempo random antes de imprimir el mensaje
  
  pid_t pidC=fork();


  if(pidC != 0){
	printf("soy el padre:%d \n", getpid());
	
	do_nothing(2000000,msg);
	do_nothing_random(msg);
	
	wait(NULL);
	

  }else if(pidC == 0){

	
	
	printf("soy el proceso hijo: %d, \n", getpid());
	
	do_nothing(2000000,msg);
			//printf("indice: %d , y funcion 2seg: %s \n ", i ,mensaje1);
			 //esperar 2 segundos, 1 millon de microsegundos en 1 segundo
	do_nothing_random(msg);	
			//printf("indice: %d , y funcion random: %s \n ", i , mensaje2);
			 //esperar un tiempo random antes de imprimir el mensaje
	
		
	
		
	
 } 
	else{/*error*/
			printf("hubo error");
 }
  




return 0;
}

//para compilar: gcc do_nothing.c -o ejecutable
//para ejecutar: ./ejecutable

 